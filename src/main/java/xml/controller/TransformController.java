package xml.controller;


import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import xml.model.Card;
import xml.model.CompairCardByYear;
import xml.model.OldCards;
import xml.model.NewRoot;
import xml.model.transform.TransformationToHTML;
import xml.model.parsers.dom.CardsDOMParser;
import xml.model.transform.TransformationToXML;

public class TransformController {
  TransformationToHTML transformation = new TransformationToHTML();
  TransformationToXML transformationToXML = new TransformationToXML();

  public void transformToHTML(File xml, String xslPath){
    try {
      transformation.transformToHTML(xslPath, getOldCards(xml));
    } catch (TransformerException e) {
      e.printStackTrace();
    } catch (JAXBException e) {
      e.printStackTrace();
    }
  }

  public void transformToHTMLWriteInFile(File xml, String xslPath){
    try {
      transformation.transformToHTMLWriteInFile(xslPath, getOldCards(xml));
    } catch (TransformerException | IOException | JAXBException e) {
      e.printStackTrace();
    }
  }

  public void transformToXML(File xml){
    try {
      transformationToXML.transformToXML(getNewRoot(xml));
    } catch (JAXBException e) {
      e.printStackTrace();
    }
  }

  private OldCards getOldCards(File xml){
    List<Card> cards = getCardList(xml);
    cards.sort(new CompairCardByYear());
    OldCards oldCards = new OldCards();
    oldCards.setCards(cards);
    return oldCards;
  }

  private NewRoot getNewRoot(File xml){
    NewRoot newRoot = new NewRoot();
    newRoot.setCards(getCardList(xml));
    return newRoot;
  }

  private List<Card> getCardList(File xml){
    CardsDOMParser cardsDOMParser = new CardsDOMParser();
    List<Card> cards = cardsDOMParser.parseFile(xml);
    return cards;
  }



}
