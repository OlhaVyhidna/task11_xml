package xml.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import xml.model.Card;
import xml.model.parsers.dom.CardsDOMParser;
import xml.model.parsers.sax.SAXCardParser;
import xml.model.parsers.staxs.CardStaxParser2;
import xml.model.parsers.staxs.CardsStaxParser;
import xml.view.CardsView;

public class ParserController {
  private CardsView cardsView = new CardsView();

  public void saxParse(File xml){
    SAXCardParser cardParser = new SAXCardParser();
    List<Card> cards = cardParser.parseFile(xml);
    cardsView.showCards(cards);
  }

  public void domParser(File xml){
    CardsDOMParser cardsDOMParser = new CardsDOMParser();
    List<Card> cards = cardsDOMParser.parseFile(xml);
    cardsView.showCards(cards);
  }

  public void staxParser(File xml){
    CardsStaxParser cardsStaxParser = new CardsStaxParser();
    List<Card> cards = cardsStaxParser.reaFile(xml);
    cardsView.showCards(cards);
  }

  public void staxParser2(File xml){
    CardStaxParser2 cardStaxParser2 = new CardStaxParser2();
    List<Card> cards = null;
    try {
      cards = cardStaxParser2.readFile(xml);
    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    cardsView.showCards(cards);

  }

}
