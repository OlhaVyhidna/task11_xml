package xml.model;

import java.util.Comparator;

public class CompairCardByYear implements Comparator<Card> {

  @Override
  public int compare(Card card1, Card card2) {
    return card1.getYear()-card2.getYear();
  }
}
