package xml.model.transform;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import xml.model.NewRoot;

public class TransformationToXML {

  public void transformToXML(NewRoot newRoot) throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(NewRoot.class);
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    StringWriter stringWriter = new StringWriter();
    marshaller.marshal(newRoot, stringWriter);

    final String xmlContext = stringWriter.toString();
    System.out.println(xmlContext);
  }

}
