package xml.model.transform;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import xml.model.OldCards;

public class TransformationToHTML {
  public void transformToHTML(String xslPath, OldCards oldCards)
      throws TransformerException, JAXBException {
    TransformerFactory factory = TransformerFactory.newInstance();
    StreamSource xslt = new StreamSource("src/main/resources/card/card.xsl");
    Transformer transformer = factory.newTransformer(xslt);

    JAXBContext context = JAXBContext.newInstance(OldCards.class);
    JAXBSource source = new JAXBSource(context, oldCards);
    StreamResult result = new StreamResult(System.out);
    transformer.transform(source, result);

  }
  public void transformToHTMLWriteInFile(String xslPath, OldCards oldCards)
      throws TransformerException, JAXBException, IOException {
    StringWriter stringWriter = new StringWriter();
    FileWriter fileWriter = new FileWriter("src/main/resources/card/write.html");

    TransformerFactory factory = TransformerFactory.newInstance();
    StreamSource xslt = new StreamSource("src/main/resources/card/card.xsl");
    Transformer transformer = factory.newTransformer(xslt);

    JAXBContext context = JAXBContext.newInstance(OldCards.class);
    JAXBSource source = new JAXBSource(context, oldCards);
    StreamResult result = new StreamResult(stringWriter);
    transformer.transform(source, result);
    fileWriter.write(stringWriter.toString());
    fileWriter.close();

  }

}
