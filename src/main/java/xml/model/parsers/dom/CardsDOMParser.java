package xml.model.parsers.dom;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xml.model.Card;

public class CardsDOMParser {

  public List<Card> parseFile(File xml) {
    Document doc = getDocument(xml);
    NodeList cardNodeList = doc.getElementsByTagName("card");
    List<Card> cards = new ArrayList<>();
    fillCardsList(cards, cardNodeList);
    return cards;
  }

  private void fillCardsList(List<Card> cards, NodeList cardNodeList) {
    for (int temp = 0; temp < cardNodeList.getLength(); temp++) {
      Node cNode = cardNodeList.item(temp);
      if (cNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eElement = (Element) cNode;
        String cardTheme = eElement.getElementsByTagName("theme").item(0).getTextContent();
        String cardType = eElement.getElementsByTagName("type").item(0).getTextContent();
        String cardWasSentString = eElement.getElementsByTagName("wasSent").item(0)
            .getTextContent();
        boolean cardWasSent = Boolean.parseBoolean(cardWasSentString);
        String cardCountry = eElement.getElementsByTagName("country").item(0).getTextContent();
        int cardYear = Integer.parseInt(eElement.getElementsByTagName("year").item(0)
            .getTextContent());
        List<String> cardAuthors = new ArrayList<>();
        NodeList author = eElement.getElementsByTagName("author");
        if (author.getLength() > 0) {
          for (int i = 0; i < author.getLength(); i++) {
            cardAuthors.add(author.item(i).getTextContent());
          }
        }
        String cardValuable = eElement.getElementsByTagName("valuable").item(0).getTextContent();
        Card currentCard = new Card(cardTheme, cardType, cardWasSent, cardCountry, cardYear,
            cardAuthors, cardValuable);
        cards.add(currentCard);
      }
    }
  }

  private Document getDocument(File xml) {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = null;
    Document parse = null;
    try {
      documentBuilder = documentBuilderFactory.newDocumentBuilder();
      parse = documentBuilder.parse(xml);
    } catch (ParserConfigurationException | SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    parse.getDocumentElement().normalize();
    return parse;
  }

}
