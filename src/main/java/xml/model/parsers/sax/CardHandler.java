package xml.model.parsers.sax;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xml.model.Card;

public class CardHandler extends DefaultHandler {
  boolean bCardTheme = false;
  boolean bCardType = false;
  boolean bCardWasSent = false;
  boolean bCardCountry = false;
  boolean bCardYear = false;
  boolean bCardAuthor = false;
  boolean bCardValuable = false;

  private List<Card> cards = new ArrayList<>();
  private Card card = null;
  private List<String> authors = null;

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    if (qName.equalsIgnoreCase("card")){
      card = new Card();
      authors = new ArrayList<>();
    } else if (qName.equalsIgnoreCase("theme")){
      bCardTheme = true;
    }else if (qName.equalsIgnoreCase("type")){
      bCardType = true;
    }else if (qName.equalsIgnoreCase("wasSent")){
      bCardWasSent = true;
    }else if (qName.equalsIgnoreCase("country")){
      bCardCountry = true;
    }else if (qName.equalsIgnoreCase("year")){
      bCardYear = true;
    }else if (qName.equalsIgnoreCase("author")){
      bCardAuthor = true;
    }else if (qName.equalsIgnoreCase("valuable")){
      bCardValuable = true;
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equalsIgnoreCase("card")){
      card.setAuthors(authors);
      cards.add(card);
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (bCardTheme){
      card.setTheme(new String(ch, start, length));
      bCardTheme = false;
    }else if (bCardType){
      card.setType(new String(ch, start, length));
      bCardType = false;
    }else if (bCardWasSent){
      card.setWasSent(Boolean.parseBoolean(new String(ch, start, length)));
      bCardWasSent = false;
    }else if (bCardCountry){
      card.setCountry(new String(ch, start, length));
      bCardCountry = false;
    }else if (bCardYear){
      card.setYear(Integer.parseInt(new String(ch, start, length)));
      bCardYear=false;
    }else if (bCardAuthor){
      authors.add(new String(ch, start, length));
      bCardAuthor = false;
    }else if (bCardValuable){
      card.setValuable(new String(ch, start, length));
      bCardValuable=false;
    }

  }

  public List<Card> getCards() {
    return cards;
  }
}
