package xml.model.parsers.sax;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import xml.model.Card;

public class SAXCardParser {

  public List<Card> parseFile(File xml) {
    SAXParserFactory factory = SAXParserFactory.newInstance();
    CardHandler cardHandler = new CardHandler();
    try {
      SAXParser saxParser = factory.newSAXParser();
      saxParser.parse(xml, cardHandler);
    } catch (ParserConfigurationException | SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return cardHandler.getCards();
  }

}
