package xml.model.parsers.staxs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.xml.sax.SAXException;
import xml.model.Card;

public class CardsStaxParser {
  boolean bCardTheme = false;
  boolean bCardType = false;
  boolean bCardWasSent = false;
  boolean bCardCountry = false;
  boolean bCardYear = false;
  boolean bCardAuthor = false;
  boolean bCardValuable = false;

  private List<Card> cards = new ArrayList<>();
  private Card card = null;
  private List<String> authors = null;


  public List<Card>  reaFile(File xml){
    XMLEventReader eventReader = getEventReader(xml);
    while (eventReader.hasNext()){
      try {
        XMLEvent event = eventReader.nextEvent();
        switch (event.getEventType()){
          case XMLStreamConstants.START_ELEMENT:{
            startElement(event);
            break;
          }case XMLStreamConstants.CHARACTERS:{
            characters(event);
            break;
          }case XMLStreamConstants.END_ELEMENT:{
            endElement(event);
          }
        }
      } catch (XMLStreamException | SAXException e) {
        e.printStackTrace();
      }
    }
    return cards;
  }



  private void startElement(XMLEvent event){
    StartElement startElement = event.asStartElement();
    String qName = startElement.getName().getLocalPart();
    if (qName.equalsIgnoreCase("card")){
      card = new Card();
      authors = new ArrayList<>();
    } else if (qName.equalsIgnoreCase("theme")){
      bCardTheme = true;
    }else if (qName.equalsIgnoreCase("type")){
      bCardType = true;
    }else if (qName.equalsIgnoreCase("wasSent")){
      bCardWasSent = true;
    }else if (qName.equalsIgnoreCase("country")){
      bCardCountry = true;
    }else if (qName.equalsIgnoreCase("year")){
      bCardYear = true;
    }else if (qName.equalsIgnoreCase("author")){
      bCardAuthor = true;
    }else if (qName.equalsIgnoreCase("valuable")){
      bCardValuable = true;
    }
  }

  private void characters(XMLEvent event){
    Characters characters = event.asCharacters();
    if (bCardTheme){
      card.setTheme(characters.getData());
      bCardTheme = false;
    }else if (bCardType){
      card.setType(characters.getData());
      bCardType = false;
    }else if (bCardWasSent){
      card.setWasSent(Boolean.parseBoolean(characters.getData()));
      bCardWasSent = false;
    }else if (bCardCountry){
      card.setCountry(characters.getData());
      bCardCountry = false;
    }else if (bCardYear){
      card.setYear(Integer.parseInt(characters.getData()));
      bCardYear=false;
    }else if (bCardAuthor){
      authors.add(characters.getData());
      bCardAuthor = false;
    }else if (bCardValuable){
      card.setValuable(characters.getData());
      bCardValuable=false;
    }
  }

  private void endElement(XMLEvent event) throws SAXException {
    EndElement endElement = event.asEndElement();
    String qName = endElement.getName().getLocalPart();
    if (qName.equalsIgnoreCase("card")){
      card.setAuthors(authors);
      cards.add(card);
    }
  }

  private XMLEventReader getEventReader(File xml) {
    XMLInputFactory factory = XMLInputFactory.newInstance();
    XMLEventReader eventReader = null;
    try {
      eventReader = factory.createXMLEventReader(new FileReader(xml));
    } catch (XMLStreamException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return eventReader;
  }

}
