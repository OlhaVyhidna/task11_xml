package xml.model.parsers.staxs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import xml.model.Card;

public class CardStaxParser2 {
  private List<Card> cards = new ArrayList<>();
  private Card card = null;
  private List<String> authors = null;

  public List<Card> readFile(File xml) throws FileNotFoundException, XMLStreamException {
    XMLInputFactory factory = XMLInputFactory.newInstance();
    XMLStreamReader streamReader = factory.createXMLStreamReader(new FileReader(xml));
    while (streamReader.hasNext()){
      streamReader.next();
      if (streamReader.getEventType()==XMLStreamReader.START_ELEMENT){
        if (streamReader.getLocalName().equalsIgnoreCase("card")){
          card = new Card();
          authors = new ArrayList<>();
        }
        if (streamReader.getLocalName().equalsIgnoreCase("theme")){
          card.setTheme(streamReader.getElementText());
        }
        if (streamReader.getLocalName().equalsIgnoreCase("type")){
          card.setType(streamReader.getElementText());
        }
        if (streamReader.getLocalName().equalsIgnoreCase("wasSent")){
          card.setWasSent(Boolean.parseBoolean(streamReader.getElementText()));
        }
        if (streamReader.getLocalName().equalsIgnoreCase("country")){
          card.setCountry(streamReader.getElementText());
        }
        if (streamReader.getLocalName().equalsIgnoreCase("year")){
          card.setYear(Integer.parseInt(streamReader.getElementText()));
        }
        if (streamReader.getLocalName().equalsIgnoreCase("author")){
          authors.add(streamReader.getElementText());
        }
        if (streamReader.getLocalName().equalsIgnoreCase("valuable")){
          card.setValuable(streamReader.getElementText());
        }
      }
      if (streamReader.getEventType()==XMLStreamReader.END_ELEMENT){
        if (streamReader.getLocalName().equalsIgnoreCase("card")){
          card.setAuthors(authors);
          cards.add(card);
        }
      }
    }
    return cards;
  }

}
