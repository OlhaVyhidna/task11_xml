package xml.model;

import java.util.List;

public class Card {
  private String theme;
  private String type;
  private boolean wasSent;
  private String country;
  private int year;
  private List<String> authors;
  private String valuable;


  public Card() {
  }

  public Card(String theme, String type, boolean wasSent, String country, int year,
      List<String> authors, String valuable) {
    this.theme = theme;
    this.type = type;
    this.wasSent = wasSent;
    this.country = country;
    this.year = year;
    this.authors = authors;
    this.valuable = valuable;
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public boolean isWasSent() {
    return wasSent;
  }

  public void setWasSent(boolean wasSent) {
    this.wasSent = wasSent;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public String getValuable() {
    return valuable;
  }

  public void setValuable(String valuable) {
    this.valuable = valuable;
  }

  @Override
  public String toString() {
    return "Card{" +
        "theme=" + theme +
        ", type='" + type + '\'' +
        ", wasSent=" + wasSent +
        ", country='" + country + '\'' +
        ", year=" + year +
        ", authors=" + authors +
        ", valuable='" + valuable + '\'' +
        '}';
  }
}
