package xml;

import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import xml.controller.ParserController;
import xml.controller.TransformController;
import xml.controller.checkxsd.CheckXSD;

public class Main {
 public static File xml = new File("src/main/resources"
      + "/card/myXML.xml");
  private static File xsd = new File("src/main/resources"
      + "/card/card.xsd");
  private static String xslPath = "src/main/resources/card/card.xsl";

  public static void main(String[] args)
      throws IOException, SAXException, IllegalAccessException, TransformerException, JAXBException {

    CheckXSD checkXSD = new CheckXSD();
    System.out.println(checkXSD.check(xml,xsd));

    ParserController parserController = new ParserController();
//    parserController.domParser(xml);
//    parserController.saxParse(xml);
    parserController.staxParser(xml);
//    parserController.staxParser2(xml);
//
    TransformController transformController = new TransformController();
    transformController.transformToHTML(xml, xslPath);
//    transformController.transformToHTMLWriteInFile(xml, xslPath);
    transformController.transformToXML(xml);




  }

}
