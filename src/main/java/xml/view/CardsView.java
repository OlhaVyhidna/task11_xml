package xml.view;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xml.model.Card;

public class CardsView {
  private static Logger LOG = LogManager.getLogger(CardsView.class);

  public void showCards(List<Card> cards){
    for (Card card : cards) {
      LOG.info(card);
    }
  }

}
