<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <html>
      <body>
        <h2>Old Cards</h2>
        <table border="1">
          <tr bgcolor="#9acd32">
            <th>Theme</th>
            <th>Type</th>
            <th>WasSent</th>
            <th>Country</th>
            <th>Year</th>
            <th>Author</th>
            <th>Valuable</th>
          </tr>
          <xsl:for-each select="OldCards/card">
            <tr>
              <td>
                <xsl:value-of select="theme"/>
              </td>
              <td>
                <xsl:value-of select="type"/>
              </td>
              <td>
                <xsl:value-of select="wasSent"/>
              </td>
              <td>
                <xsl:value-of select="country"/>
              </td>
              <td>
                <xsl:value-of select="year"/>
              </td>
              <td>
                <xsl:for-each select="author">
                  -<xsl:copy-of select="."/>
                  <br/>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="valuable"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>

  </xsl:template>
</xsl:stylesheet>
